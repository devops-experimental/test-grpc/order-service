package handler

import (
	"context"
	"fmt"
	"github.com/micro/micro/v3/service"
	pb "gitlab.com/devops-experimental/test-grpc/proto"
)

type OrderHandler struct{}

func (s *OrderHandler) CreateOrder(ctx context.Context, request *pb.CreateOrderRequest, order *pb.Order) error {
	// create and initialise a new service
	srv := service.New()

	// create the proto client
	client := pb.NewProductService("product-service", srv.Client())

	rsp, err := client.GetProductById(context.Background(), &pb.GetProductByIdRequest{
		ProductId: request.ProductId,
	})
	if err != nil {
		fmt.Println("Failed to get product from Product Service: ", err)
	}

	order.Id = request.OrderId
	order.Name = "Example Order"
	order.Product = rsp

	return nil
}
