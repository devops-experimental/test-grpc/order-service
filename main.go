package main

import (
	"fmt"
	"github.com/micro/micro/v3/service"
	"github.com/micro/micro/v3/service/logger"
	pb "gitlab.com/devops-experimental/test-grpc/proto"
	"order_service/handler"
)

func main() {
	// Create service
	srv := service.New(
		service.Name("order-service"),
	)

	// Register Handler
	pb.RegisterOrderServiceHandler(srv.Server(), &handler.OrderHandler{})

	fmt.Println("Order service is running...")

	// Run the service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
